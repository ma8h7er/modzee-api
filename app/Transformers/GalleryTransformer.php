<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class GalleryTransformer extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'img' => url(config('filesystem.photo_url_prefix', 'img/') . $this->thumbnail),
            'featured' => $this->featured,
            'date' => $this->publish_date,
        ];
    }
}
