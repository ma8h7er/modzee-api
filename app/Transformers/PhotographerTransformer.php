<?php

namespace App\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class PhotographerTransformer extends JsonResource
{
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'bio' => $this->bio,
            'profile_picture' => url(config('filesystem.photo_url_prefix', 'img/') . $this->profile_picture),
            'album' => GalleryTransformer::collection($this->galleries)
        ];
    }
}
