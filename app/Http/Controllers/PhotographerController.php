<?php

namespace App\Http\Controllers;

use App\Models\Photographer;
use App\Transformers\PhotographerTransformer;
use Illuminate\Http\Response;

class PhotographerController extends Controller
{
    /**
     * Get the photographer profile by id as JSON response
     *
     * @param $photographerId
     * @return PhotographerTransformer|\Illuminate\Http\JsonResponse
     */
    public function getProfile($photographerId)
    {
        $photographer = Photographer::findOrFail($photographerId);
        if(! $photographer) {
            return response()->json([
                'message' => 'Photographer not found!'
            ], Response::HTTP_NOT_FOUND);
        }
        return new PhotographerTransformer($photographer);
    }
}
