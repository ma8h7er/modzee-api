<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photographer extends Model
{
    use HasFactory;

    protected $table = 'photographers';

    protected $fillable = ['name', 'phone', 'email', 'bio', 'profile_picture'];

    /**
     * Get galleries of the photographer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function galleries()
    {
        return$this->hasMany(Gallery::class, 'photographer_id', 'id');
    }
}
