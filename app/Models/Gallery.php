<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;

    protected $table = 'galleries';
    protected $fillable = ['title', 'description', 'thumbnail', 'featured', 'publish_date', 'photographer_id'];

    /**
     * Get all photos in the gallery
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function photos()
    {
        return $this->belongsToMany(Photo::class, 'gallery_photos', 'gallery_id', 'photo_id')
            ->withTimestamps();
    }
}
