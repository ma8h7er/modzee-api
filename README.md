
## Photographer profile test for Modzee

This app created as a test demo for a photographer profile by Maher Alhammoud


### Installation

- **Install dependencies**

```
composer install
```

- **Database configuration**

First you need to create ```.env``` file or copy from ```.env.example```

```
cp .env.example .env
```

Then configure database in the new file ```.env```

 - **Database migration**
 
 Migrate DB tables by: ```php artisan migrate```
  
 If you need the demo data ```php artisan seed```
