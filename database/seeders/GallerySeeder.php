<?php


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $galleries = [
            [
                'id' => 1,
                'title' => 'Nandhaka Pieris',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'thumbnail' => 'landscape1.jpeg',
                'publish_date' => '2015-05-01',
                'featured' => true,
                'photographer_id' => 1
            ],
            [
                'id' => 2,
                'title' => 'New West Calgary',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'thumbnail' => 'landscape2.jpeg',
                'publish_date' => '2016-05-01',
                'featured' => false,
                'photographer_id' => 1
            ],
            [
                'id' => 3,
                'title' => 'Australian Landscape',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'thumbnail' => 'landscape3.jpeg',
                'publish_date' => '2015-02-02',
                'featured' => false,
                'photographer_id' => 1
            ],
            [
                'id' => 4,
                'title' => 'Halvergate Marsh',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'thumbnail' => 'landscape4.jpeg',
                'publish_date' => '2014-04-01',
                'featured' => true,
                'photographer_id' => 1
            ],
            [
                'id' => 5,
                'title' => 'Rikkis Landscape',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'thumbnail' => 'landscape5.jpeg',
                'publish_date' => '2010-09-01',
                'featured' => false,
                'photographer_id' => 1
            ],
            [
                'id' => 6,
                'title' => 'Kiddi Kristjans Iceland',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                'thumbnail' => 'landscape6.jpeg',
                'publish_date' => '2015-07-21',
                'featured' => true,
                'photographer_id' => 1
            ]
        ];

        DB::table('galleries')->insert($galleries);
    }
}
