<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleryPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_photos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('gallery_id');
            $table->unsignedBigInteger('photo_id');
            $table->unique(['gallery_id', 'photo_id']);
            $table->timestamps();
            $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('cascade');
            $table->foreign('photo_id')->references('id')->on('photos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gallery_photos', function (Blueprint $table) {
            $table->dropForeign('gallery_photos_gallery_id_foreign');
            $table->dropColumn('gallery_id');
            $table->dropForeign('gallery_photos_photo_id_foreign');
            $table->dropColumn('photo_id');
        });
        Schema::dropIfExists('gallery_photos');
    }
}
