<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Public API Routes
|--------------------------------------------------------------------------
| In this demo app, the api routs don't need a middleware to check user token
|
*/

Route::get('/photographer/{photographerId}', [
        'uses' => 'PhotographerController@getProfile'
    ]);
